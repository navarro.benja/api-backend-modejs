'use strict'

// Cargar modulos
var express = require('express');
var bodyParser = require('body-parser');

// ejecutar express
var app = express();

// cargar ficheros de rutas
let articlesRoutes = require('./routes/article');

// Middelwares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


// Cargar rutas
app.use('/api', articlesRoutes);

// Error middelware
app.use((err, req, res, next) => {
  if(!err.message)
    err = {message: err};
  res.status(err.status || 500).send({
    status: 'error',
    message: err.message
  });
});

// ruta de prueba
app.get('/p', (req, res) => {
  res.status(200).send({ message: "esto es una prueba" });
});



module.exports = app;