'use strict'

const validator = require('validator'),
      Article = require('../models/article'),
      fs = require('fs'),
      path = require('path');

Array.prototype.last = function (x = 1) {
  const max = this.length,
        start = max - x,
        r = [];

  for (let i = start; i < max; i++) {
    if (this[i] !== undefined)
      r.push(this[i]);
  }
  return r;
};

let controller = {
  
  datosCurso: (req, res) => {
    let hola = req.body.hola;
    return res.status(200).send({
      message: "esto es una prueba",
      hola
    });
  },

  test: (req, res) => {
    return res.status(200).send({
      message: "Show la accion test del controlador de ariculos"
    });
  },

  save: (req, res, next) => {
    // REcoger parametros por post
    let params = req.body;

    // Validar datos
    try {
      var validate_title = !validator.isEmpty(params.title),
          validate_content = !validator.isEmpty(params.content);
      
    } catch(err) {
      next({
        status: '422',
        message: "Faltan datos"
      });
    }

    // Crear objeto
    let article = new Article();

    // Asignar valores
    article.title = params.title;
    article.content = params.content;
    article.image = null;

    // Guardar articulo
    article.save((err, saved) => {
      if (err || !saved)
        return res.status(404).send({
          status: 'error',
          message: 'Los datos no se han guardado'
        });
      return res.status(201).send({
        status: 'ok',
        article
      });

    });
  },

  getArticles: (req, res) => {
    let last = req.params.last,
        query = Article.find();
    
    if (last)
    {
      query.limit(Number(last) || 10);
    }
    // buscar datos
    query.sort('-_id').exec((err, articles) => {
      if (err || !articles)
        throw({
          status: '500',
          message: 'No se han encontrado datos'
        });

      return res.status(200).send({
        status: 'ok',
        articles
      });
    });
  },

  getArticle: (req, res, next) => {
    let id = req.params.id;

    Article.findById(id)
      .then(article => {
        if(!article)
          throw({
            status: 404,
            message: 'article nor found'
          });
        res.status(200).send({
          status: 'ok',
          article
        });
      })
      .catch(next);
  },

  update: (req, res, next) => {
    const articleId = req.params.id,
          params = req.body;

    try {

      validator.isEmpty(params.title),
      validator.isEmpty(params.content);
        
    } catch (err) {

      throw({
        status: 'error',
        message: "Faltan datos"
      });

    }

    Article.findByIdAndUpdate(articleId, params, {returnDocument: 'after'})
      .then(article => {
        if (!article)
          throw({status: 404, message: 'article not found'});

        res.status(200).send({
          status: 'ok',
          article
        });
      })
      .catch(next);
  },

  delete: (req, res, next) => {
    Article.findByIdAndDelete(req.params.id)
      .then(article => {
        if (!article)
          throw({status: 404, message: 'article not found'});

        res.status(410).send({
          status: 'ok',
          article
        });
      })
      .catch(next);
  },

  upload: (req, res, next) => {
    let file_name = 'noname',
        file_path;

    const articleId = req.params.id;

    function deleteFiles(ignore) {
      if (!req.files)
        return;
      for (let [field, value] of Object.entries(req.files))
      {
        if (field !== ignore)
          fs.unlink(value.path, (err) => {
            if (err) console.log(err);
          });
      }
    }

    try {
      if (!req.files || !req.files.file0)
        throw({status: 422, message: 'File unspecified'});

      if (req.files.file0.type.indexOf('image') !== 0)
        throw({status: 422, message: 'Invalid image format'});
    } catch (err) {
      deleteFiles();
      return next(err);
    }

    deleteFiles('file0'); // borra los demas campos de archivos menos el file0

    file_name = req.files.file0.path.split('/').last()[0];

    Article.findByIdAndUpdate(articleId, {image: file_name}, {returnDocument: 'after'})
      .then(article => {
        if (!article)
          throw({status: 404, message: 'article not found'});

        res.status(200).send({
          status: 'ok',
          article
        });
      })
      .catch(next);
  },

  getImage: (req, res) => {
    const filename = req.params.image;

    if (fs.statSync('./uploads/' + filename, {throwIfNoEntry: false})) {
      res.sendFile(path.resolve('./uploads/' + filename));
    }
    else {
      throw({status: 404, message: "File not found"});
    }
  },

  search: (req, res, next) => {
    const q = req.params.search;

    Article.find({
        "$or": [
          { "title": { "$regex": q, "$options": "i" }},
          { "content": { "$regex": q, "$options": "i" }}
        ]
      })
      .sort('-date')
      .exec()
      .then(articles => {
        res.status(200).send({
          status: 'ok',
          articles
        });
      })
      .catch(next);
  }

};

module.exports = controller;
