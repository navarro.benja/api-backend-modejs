'use strict'

let express = require('express'),
    articleController = require('../controllers/article'),
    md_upload = require('connect-multiparty')({uploadDir: './uploads'}),
    router = express.Router();
console.log(md_upload);
//Rutas prueba
router.get('/text', articleController.test);
router.post('/datos',articleController.datosCurso);

// Rutas para alticulos
router.post('/save', articleController.save);
router.get('/articles/:last?', articleController.getArticles);
router.get('/article/:id', articleController.getArticle);
router.put('/article/:id', articleController.update);
router.delete('/article/:id', articleController.delete);
router.post('/upload/:id', md_upload, articleController.upload);
router.get('/get-image/:image', articleController.getImage);
router.get('/search/:search', articleController.search);

module.exports = router;